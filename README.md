# Active Directory

A Node Package for interfacing with Active Directory

### Install using npm

```
npm install bitbucket:vmattgowland/active-directory
```



#API Documentation:

## ActiveDirectory : Object
**Kind**: global class
**Properties**

| Name | Type |
| --- | --- |
| username | string |
| password | string |
| domain | string |
| baseDN | string |
| url | string |


* [ActiveDirectory](#markdown-header-activedirectory-object) : Object
    * [new ActiveDirectory(url, baseDN, domain, username, password)](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
    * _instance_
        * [.bind()](#markdown-header-activedirectorybind)
        * [.authenticate(username, password)](#markdown-header-activedirectoryauthenticateusername-password-boolean) ⇒ boolean
        * [.getDirectGroupMembers(groupname)](#markdown-header-activedirectorygetdirectgroupmembersgroupname)
        * [.getGroupUsers(groupname)](#markdown-header-activedirectorygetgroupusersgroupname)
        * [.getUser(username)](#markdown-header-activedirectorygetuserusername-user) ⇒ User
        * [.getGroup(groupname)](#markdown-header-activedirectorygetgroupgroupname-group) ⇒ Group
        * [.getObjectByDN(dn)](#markdown-header-activedirectorygetobjectbydndn-object) ⇒ object
        * [.userIsMemberOf(username, groupname)](#markdown-header-activedirectoryuserismemberofusername-groupname)
    * _static_
        * [.createClient(url, baseDN, domain, username, password)](#markdown-header-activedirectorycreateclienturl-basedn-domain-username-password-object) ⇒ object

### new ActiveDirectory(url, baseDN, domain, username, password)
Creates an Active Directory connection object


| Param | Type | Description |
| --- | --- | --- |
| url | string | URL to Domain Controller |
| baseDN | string | Base objects Distringuished Name |
| domain | string | Domain prefix for user authentication eg: D230119 |
| username | string | SAMAccountName of user to bind to Active Directory eg: AB1234 |
| password | string | Password to bind to Active Directory |

### activeDirectory.bind()
Attempts to bind this ActiveDirectory connection object to the Active Directory server

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Throws**:

- LDAPError If failed to bind to AD.

### activeDirectory.authenticate(username, password) ⇒ boolean
Authenticates a username/password. Performs a simple ldap bind with the supplied username/password

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Returns**: boolean - true if authentication is successful.

| Param | Type | Description |
| --- | --- | --- |
| username | string | SAMAccountName of user |
| password | string | Users |

### activeDirectory.getDirectGroupMembers(groupname)
Gets the direct members of a group.  Does not follow child groups

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)

| Param | Type | Description |
| --- | --- | --- |
| groupname | string | Name of LDAP group |

### activeDirectory.getGroupUsers(groupname)
Gets all users in a group. Follows child groups recusively. Returns users only.

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)

| Param | Type | Description |
| --- | --- | --- |
| groupname | groupname | Name of group ldap group |

### activeDirectory.getUser(username) ⇒ User
Gets a User from Active Directory

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Returns**: User - A User object or null if no user is found

| Param | Type | Description |
| --- | --- | --- |
| username | string | SAMAccountName of user to fetch |

### activeDirectory.getGroup(groupname) ⇒ Group
Gets a Group from Active Directory

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Returns**: Group - A Group object

| Param | Type | Description |
| --- | --- | --- |
| groupname | string | SAMAccountName of group |

### activeDirectory.getObjectByDN(dn) ⇒ object
Gets the AccountName and Class of a given Distringuished Name

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Returns**: object - An object with props: name, objectType(user, group, contact), SAMAccountName

| Param | Type | Description |
| --- | --- | --- |
| dn | string | The Distinguished Name of an object in Active Directory |

### activeDirectory.userIsMemberOf(username, groupname)
Checks whether a user is a member of a group. Checks nested groups.

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)

| Param | Type | Description |
| --- | --- | --- |
| username | string | SAMAccountName of user |
| groupname | string | SAMAccountName of group |

### ActiveDirectory.createClient(url, baseDN, domain, username, password) ⇒ object
Static method returns an Active Direcotry connection object AND binds to the domain.

**Kind**: static method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Returns**: object - Active Directory object if successfully binded
**Throws**:

- LDAPError If failed to bind to Active Directory


| Param | Type | Description |
| --- | --- | --- |
| url | string | URL to Domain Controller |
| baseDN | string | Base objects Distringuished Name |
| domain | string | Domain prefix for user authentication eg: D230119 |
| username | string | SAMAccountName of user to bind to Active Directory eg: AB1234 |
| password | string | Password to bind to Active Directory |

## ActiveDirectory
**Kind**: global class

* [ActiveDirectory](#markdown-header-activedirectory)
    * [new ActiveDirectory(url, baseDN, domain, username, password)](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
    * _instance_
        * [.bind()](#markdown-header-activedirectorybind)
        * [.authenticate(username, password)](#markdown-header-activedirectoryauthenticateusername-password-boolean) ⇒ boolean
        * [.getDirectGroupMembers(groupname)](#markdown-header-activedirectorygetdirectgroupmembersgroupname)
        * [.getGroupUsers(groupname)](#markdown-header-activedirectorygetgroupusersgroupname)
        * [.getUser(username)](#markdown-header-activedirectorygetuserusername-user) ⇒ User
        * [.getGroup(groupname)](#markdown-header-activedirectorygetgroupgroupname-group) ⇒ Group
        * [.getObjectByDN(dn)](#markdown-header-activedirectorygetobjectbydndn-object) ⇒ object
        * [.userIsMemberOf(username, groupname)](#markdown-header-activedirectoryuserismemberofusername-groupname)
    * _static_
        * [.createClient(url, baseDN, domain, username, password)](#markdown-header-activedirectorycreateclienturl-basedn-domain-username-password-object) ⇒ object

### new ActiveDirectory(url, baseDN, domain, username, password)
Creates an Active Directory connection object


| Param | Type | Description |
| --- | --- | --- |
| url | string | URL to Domain Controller |
| baseDN | string | Base objects Distringuished Name |
| domain | string | Domain prefix for user authentication eg: D230119 |
| username | string | SAMAccountName of user to bind to Active Directory eg: AB1234 |
| password | string | Password to bind to Active Directory |

### activeDirectory.bind()
Attempts to bind this ActiveDirectory connection object to the Active Directory server

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Throws**:

- LDAPError If failed to bind to AD.

### activeDirectory.authenticate(username, password) ⇒ boolean
Authenticates a username/password. Performs a simple ldap bind with the supplied username/password

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Returns**: boolean - true if authentication is successful.

| Param | Type | Description |
| --- | --- | --- |
| username | string | SAMAccountName of user |
| password | string | Users |

### activeDirectory.getDirectGroupMembers(groupname)
Gets the direct members of a group.  Does not follow child groups

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)

| Param | Type | Description |
| --- | --- | --- |
| groupname | string | Name of LDAP group |

### activeDirectory.getGroupUsers(groupname)
Gets all users in a group. Follows child groups recusively. Returns users only.

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)

| Param | Type | Description |
| --- | --- | --- |
| groupname | groupname | Name of group ldap group |

### activeDirectory.getUser(username) ⇒ User
Gets a User from Active Directory

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Returns**: User - A User object or null if no user is found

| Param | Type | Description |
| --- | --- | --- |
| username | string | SAMAccountName of user to fetch |

### activeDirectory.getGroup(groupname) ⇒ Group
Gets a Group from Active Directory

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Returns**: Group - A Group object

| Param | Type | Description |
| --- | --- | --- |
| groupname | string | SAMAccountName of group |

### activeDirectory.getObjectByDN(dn) ⇒ object
Gets the AccountName and Class of a given Distringuished Name

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Returns**: object - An object with props: name, objectType(user, group, contact), SAMAccountName

| Param | Type | Description |
| --- | --- | --- |
| dn | string | The Distinguished Name of an object in Active Directory |

### activeDirectory.userIsMemberOf(username, groupname)
Checks whether a user is a member of a group. Checks nested groups.

**Kind**: instance method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)

| Param | Type | Description |
| --- | --- | --- |
| username | string | SAMAccountName of user |
| groupname | string | SAMAccountName of group |

### ActiveDirectory.createClient(url, baseDN, domain, username, password) ⇒ object
Static method returns an Active Direcotry connection object AND binds to the domain.

**Kind**: static method of [ActiveDirectory](#markdown-header-new-activedirectoryurl-basedn-domain-username-password)
**Returns**: object - Active Directory object if successfully binded
**Throws**:

- LDAPError If failed to bind to Active Directory


| Param | Type | Description |
| --- | --- | --- |
| url | string | URL to Domain Controller |
| baseDN | string | Base objects Distringuished Name |
| domain | string | Domain prefix for user authentication eg: D230119 |
| username | string | SAMAccountName of user to bind to Active Directory eg: AB1234 |
| password | string | Password to bind to Active Directory |

## User : Object
**Kind**: global typedef
**Properties**

| Name | Type |
| --- | --- |
| username | string |
| distinguishedName | string |
| firstname | string |
| lastname | string |
| name | string |
| email | string |
| branch | string |
| telephone | string |
| ipPhone | string |
| mobilephone | string |
| title | string |
| department | string |

## Group : Object
**Kind**: global typedef
**Properties**

| Name | Type |
| --- | --- |
| name | string |
| dn | string |
| type | string |
| email | string |
| members | string |
