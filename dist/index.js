"use strict";

var ldapjs = _interopRequireWildcard(require("ldapjs"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const LDAP_CODES = {
  invalid_credentials: 49
};
const LDAP_RETURN_ATTRIBUTES = {
  user: ["sAMAccountName", "cn", "distinguishedName", "givenName", "sn", "name", "mail", "physicalDeliveryOfficeName", "telephoneNumber", "ipphone", "mobile", "title", "department"],
  group: ["mail", "groupType", "member"],
  object: ["cn", "distinguishedName", "samaccountname", "objectClass", "objectCategory"]
};
/**
 * API for Active Directory
 */

/**
 * @typedef {Object} User
 * @property {string} username
 * @property {string} distinguishedName
 * @property {string} firstname
 * @property {string} lastname
 * @property {string} name
 * @property {string} email
 * @property {string} branch
 * @property {string} telephone
 * @property {string} ipPhone
 * @property {string} mobilephone
 * @property {string} title
 * @property {string} department
 */

/**
 * @typedef {Object} Group
 * @property {string} name
 * @property {string} dn
 * @property {string} type
 * @property {string} email
 * @property {string} members
 */

/**
 *  @class ActiveDirectory
 *  @type {Object}
 *  @property {string} username
 *  @property {string} password
 *  @property {string} domain
 *  @property {string} baseDN
 *  @property {string} url
 */

module.exports = class ActiveDirectory {
  /**
   * Creates an Active Directory connection object
   * @param {string} url URL to Domain Controller
   * @param {string} baseDN Base objects Distringuished Name
   * @param {string} domain Domain prefix for user authentication eg: D230119
   * @param {string} username SAMAccountName of user to bind to Active Directory eg: AB1234
   * @param {string} password Password to bind to Active Directory
   */
  constructor(url, baseDN, domain, username, password) {
    _defineProperty(this, "_client", void 0);

    _defineProperty(this, "_username", void 0);

    _defineProperty(this, "_password", void 0);

    _defineProperty(this, "_domain", void 0);

    _defineProperty(this, "_baseDN", void 0);

    _defineProperty(this, "_url", void 0);

    _defineProperty(this, "_isConnected", void 0);

    this._url = url;
    this._baseDN = baseDN;
    this._username = username;
    this._domain = domain;
    this._password = password;
    this._isConnected = false;
    this._client = ldapjs.createClient({
      url: url,
      reconnect: true
    });
  }
  /**
   * Static method returns an Active Direcotry connection object AND binds to the domain.
   * @param {string} url URL to Domain Controller
   * @param {string} baseDN Base objects Distringuished Name
   * @param {string} domain Domain prefix for user authentication eg: D230119
   * @param {string} username SAMAccountName of user to bind to Active Directory eg: AB1234
   * @param {string} password Password to bind to Active Directory
   * @returns {object} Active Directory object if successfully binded
   * @throws {LDAPError} If failed to bind to Active Directory
   */


  static createClient(url, baseDN, domain, username, password) {
    var _this = this;

    return _asyncToGenerator(function* () {
      try {
        let newClient = new _this(url, baseDN, domain, username, password);
        yield newClient.bind();
        return newClient;
      } catch (error) {
        throw error;
      }
    })();
  }
  /**
   * Attempts to bind this ActiveDirectory connection object to the Active Directory server
   * @throws {LDAPError} If failed to bind to AD.
   */


  bind() {
    var _this2 = this;

    return _asyncToGenerator(function* () {
      let fqn = `${_this2._domain}\\${_this2._username}`;

      _this2._client.on("error",
      /*#__PURE__*/
      function () {
        var _ref = _asyncToGenerator(function* (err) {
          console.log(err.message);
          _this2._isConnected = false;
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());

      return new Promise((resolve, reject) => {
        let callback = err => {
          if (err) {
            reject(err);
          } else {
            _this2._isConnected = true;
            resolve();
          }
        };

        _this2._client.bind(fqn, _this2._password, callback);
      });
    })();
  }
  /**
   * Authenticates a username/password. Performs a simple ldap bind with the supplied username/password
   * @param {string} username SAMAccountName of user
   * @param {string} password Users
   * @returns {boolean} true if authentication is successful.
   */


  authenticate(username, password) {
    var _this3 = this;

    return _asyncToGenerator(function* () {
      try {
        _this3._isConnected || (yield _this3.bind());
      } catch (err) {
        throw new Error("Failed to bind to Active Directory: " + err.message);
      }

      return new Promise((resolve, reject) => {
        let tempClient = ldapjs.createClient({
          url: _this3._url
        });

        let callback = (err, res) => {
          if (err) {
            if (err.code === LDAP_CODES.invalid_credentials) {
              resolve(false);
            } else {
              reject(`Error authenticating user: Error Name: ${err.name}, LDAP Error Code ${err.code}, Message: ${err.message}`);
            }
          } else {
            resolve(true);
          }
        };

        let fqn = `${_this3._domain}\\${username}`;
        tempClient.bind(fqn, password, callback);
      });
    })();
  }
  /**
   * Gets the direct members of a group.  Does not follow child groups
   * @param {string} groupname Name of LDAP group
   */


  getDirectGroupMembers(groupname) {
    var _this4 = this;

    return _asyncToGenerator(function* () {
      try {
        _this4._isConnected || (yield _this4.bind());
      } catch (err) {
        throw new Error("Failed to bind to Active Directory: " + err.message);
      }

      try {
        let members = [];
        let group = yield _this4.getGroup(groupname);
        if (!group || !group.members || !group.members.length) return null;

        for (var index = 0; index < group.members.length; index++) {
          var memberDN = group.members[index];
          members.push((yield _this4.getObjectByDN(memberDN)));
        }

        return members;
      } catch (error) {
        console.log(`Failed to retrive members of group ${groupname}. Reason: ${error.message}`);
        throw error;
      }
    })();
  }
  /**
   * Gets all users in a group. Follows child groups recusively. Returns users only.
   * @param {groupname} groupname Name of group ldap group
   */


  getGroupUsers(groupname) {
    var _this5 = this;

    return _asyncToGenerator(function* () {
      try {
        _this5._isConnected || (yield _this5.bind());
      } catch (err) {
        throw new Error("Failed to bind to Active Directory: " + err.message);
      }

      try {
        let users = [];
        let members = yield _this5.getDirectGroupMembers(groupname);
        if (!members || !members.length) return users;

        for (var index = 0; index < members.length; index++) {
          var member = members[index];

          if (member.objectType === LdapObjectType.user) {
            users.push((yield _this5.getUser(member.accountName)));
          } else if (member.objectType === LdapObjectType.group) {
            users = users.concat((yield _this5.getGroupUsers(member.accountName)));
          }
        }

        let usernames = [];
        let noDuplicates = [];
        users.forEach(user => {
          if (usernames.indexOf(user.username) === -1) {
            usernames.push(user.username);
            noDuplicates.push(user);
          }
        });
        return noDuplicates;
      } catch (error) {
        console.log(`Failed to get users of group ${groupname}. Reason: ${error.message}`);
        throw error;
      }
    })();
  }
  /**
   * Gets a User from Active Directory
   * @param {string} username SAMAccountName of user to fetch
   * @returns {User} A User object or null if no user is found
   */


  getUser(username) {
    var _this6 = this;

    return _asyncToGenerator(function* () {
      if (!username) return null;

      try {
        _this6._isConnected || (yield _this6.bind());
      } catch (err) {
        throw new Error("Failed to bind to Active Directory: " + err.message);
      }

      try {
        let ldapquery = `(&(objectCategory=person)(objectClass=user)(samaccountname=${username}))`;
        let response = yield _this6.search(ldapquery, LDAP_RETURN_ATTRIBUTES.user);
        if (!response.length) return null;
        if (response.length > 1) throw {
          message: "GetUser query returned more than one user.  This is.. not posible.",
          result: response
        };
        let user = {
          username: response[0].sAMAccountName,
          distinguishedName: response[0].distinguishedName,
          firstname: response[0].givenName,
          lastname: response[0].sn,
          name: response[0].name,
          email: response[0].mail,
          branch: response[0].physicalDeliveryOfficeName,
          telephone: response[0].telephoneNumber,
          ipPhone: response[0].ipPhone,
          mobilephone: response[0].mobile,
          title: response[0].title,
          department: response[0].department
        };
        return user;
      } catch (error) {
        console.log("getUser() ran into an error");
        console.log(error); //todo handle error
      }
    })();
  }
  /**
   * Gets a Group from Active Directory
   * @param {string} groupname SAMAccountName of group
   * @returns {Group} A Group object
   */


  getGroup(groupname) {
    var _this7 = this;

    return _asyncToGenerator(function* () {
      if (!groupname) return null;

      try {
        _this7._isConnected || (yield _this7.bind());
      } catch (err) {
        throw new Error("Failed to bind to Active Directory: " + err.message);
      }

      try {
        let ldapquery = `(&(objectCategory=group)(cn=${groupname}))`;
        let response = yield _this7.search(ldapquery, LDAP_RETURN_ATTRIBUTES.group);
        if (!response || !response.length) return null;
        if (response.length > 1) throw {
          message: "GetGroup query returned more than one group.  This is.. not posible.",
          result: response
        };
        let members = response[0].member;

        if (members === null || members === undefined) {
          members = [];
        } else if (!Array.isArray(members)) {
          members = [members];
        }

        let group = {
          dn: response[0].distinguishedName,
          type: response[0].groupType,
          email: response[0].sn,
          name: response[0].name,
          members: members
        };
        return group;
      } catch (error) {
        console.log("getGroup() ran into an error");
        console.log(error); //todo handle error
      }
    })();
  }
  /**
   * Gets the AccountName and Class of a given Distringuished Name
   * @param {string} dn The Distinguished Name of an object in Active Directory
   * @returns {object} An object with props: name, objectType(user, group, contact), SAMAccountName
   */


  getObjectByDN(dn) {
    var _this8 = this;

    return _asyncToGenerator(function* () {
      if (!dn) return null;

      try {
        _this8._isConnected || (yield _this8.bind());
      } catch (err) {
        throw new Error("Failed to bind to Active Directory: " + err.message);
      }

      try {
        let ldapquery = `(distinguishedName=${dn})`;
        let response = yield _this8.search(ldapquery, LDAP_RETURN_ATTRIBUTES.object);
        if (!response.length) return null;
        if (response.length > 1) throw {
          message: "getObjectByDN query returned more than one object.  This is.. not posible.",
          result: response
        };
        let thing = {
          name: response[0].cn,
          objectType: _this8.determineObjectType(response[0].objectCategory, response[0].objectClass),
          accountName: response[0].sAMAccountName
        };
        return thing;
      } catch (error) {
        console.log("getObjectByDN() ran into an error");
        console.log(error); //todo handle error
      }
    })();
  }

  determineObjectType(ldapCategory, ldapClass) {
    let paths = ldapCategory.split(",");
    let cn = paths[0].split("=")[1];
    if (cn === "Person" && ldapClass.indexOf("user") > -1) return LdapObjectType.user;else if (cn === "Person" && ldapClass.indexOf("contact") > -1) return LdapObjectType.contact;else if (cn === "Computer") return LdapObjectType.computer;else if (cn === "Group") return LdapObjectType.group;else if (cn === "OU") return LdapObjectType.ou;else console.log("determineObjectType() couldnt determine a valid known object type. We only support a few main ones. returning null.");
    return null;
  }
  /**
   * Checks whether a user is a member of a group. Checks nested groups.
   * @param {string} username SAMAccountName of user
   * @param {string} groupname SAMAccountName of group
   */


  userIsMemberOf(username, groupname) {
    var _this9 = this;

    return _asyncToGenerator(function* () {
      try {
        _this9._isConnected || (yield _this9.bind());
      } catch (err) {
        throw new Error("Failed to bind to Active Directory: " + err.message);
      }

      let users = yield _this9.getGroupUsers(groupname);

      for (var index = 0; index < users.length; index++) {
        var user = users[index];

        if (user.username.toUpperCase() === username.trim().toUpperCase()) {
          return true;
        }
      }

      return false;
    })();
  }

  search(ldapquery, returnAttributes, baseDN, scope) {
    var _this10 = this;

    if (!scope) {
      scope = "sub";
    }

    let options = {
      scope: scope,
      filter: ldapquery,
      attributes: returnAttributes
    };
    let controls;
    return new Promise(
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(function* (resolve, reject) {
        let result = [];

        let callback = (err, response) => {
          if (err) {
            reject(err);
          } else {
            response.on("searchEntry", function (entry) {
              result.push(entry.object);
            });
            response.on("error", function (err) {
              reject(err);
            });
            response.on("end", function (end) {
              if (end.status === 0) {
                resolve(result);
              } else {
                let error = {
                  status: end.status,
                  message: "LDAP search ended unsuccessfully (non 0 status)"
                };
                reject(error);
              }
            });
          }
        };

        try {
          _this10._isConnected || (yield _this10.bind());
        } catch (err) {
          throw new Error("Failed to bind to Active Directory: " + err.message);
        }

        _this10._client.search(baseDN || _this10._baseDN, options, callback);
      });

      return function (_x2, _x3) {
        return _ref2.apply(this, arguments);
      };
    }());
  }

};