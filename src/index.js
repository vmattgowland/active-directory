import * as ldapjs from "ldapjs";

const LDAP_CODES = {
  invalid_credentials: 49
};

const LDAP_RETURN_ATTRIBUTES = {
  user: [
    "sAMAccountName",
    "cn",
    "distinguishedName",
    "givenName",
    "sn",
    "name",
    "mail",
    "physicalDeliveryOfficeName",
    "telephoneNumber",
    "ipphone",
    "mobile",
    "title",
    "department"
  ],
  group: ["mail", "groupType", "member"],
  object: ["cn", "distinguishedName", "samaccountname", "objectClass", "objectCategory"]
};
/**
 * API for Active Directory
 */

/**
 * @typedef {Object} User
 * @property {string} username
 * @property {string} distinguishedName
 * @property {string} firstname
 * @property {string} lastname
 * @property {string} name
 * @property {string} email
 * @property {string} branch
 * @property {string} telephone
 * @property {string} ipPhone
 * @property {string} mobilephone
 * @property {string} title
 * @property {string} department
 */

/**
 * @typedef {Object} Group
 * @property {string} name
 * @property {string} dn
 * @property {string} type
 * @property {string} email
 * @property {string} members
 */

/**
 *  @class ActiveDirectory
 *  @type {Object}
 *  @property {string} username
 *  @property {string} password
 *  @property {string} domain
 *  @property {string} baseDN
 *  @property {string} url
 */
module.exports = class ActiveDirectory {
  _client;
  _username;
  _password;
  _domain;
  _baseDN;
  _url;
  _isConnected;

  /**
   * Creates an Active Directory connection object
   * @param {string} url URL to Domain Controller
   * @param {string} baseDN Base objects Distringuished Name
   * @param {string} domain Domain prefix for user authentication eg: D230119
   * @param {string} username SAMAccountName of user to bind to Active Directory eg: AB1234
   * @param {string} password Password to bind to Active Directory
   */
  constructor(url, baseDN, domain, username, password) {
    this._url = url;
    this._baseDN = baseDN;
    this._username = username;
    this._domain = domain;
    this._password = password;
    this._isConnected = false;
    this._client = ldapjs.createClient({
      url: url,
      reconnect: true
    });
  }

  /**
   * Static method returns an Active Direcotry connection object AND binds to the domain.
   * @param {string} url URL to Domain Controller
   * @param {string} baseDN Base objects Distringuished Name
   * @param {string} domain Domain prefix for user authentication eg: D230119
   * @param {string} username SAMAccountName of user to bind to Active Directory eg: AB1234
   * @param {string} password Password to bind to Active Directory
   * @returns {object} Active Directory object if successfully binded
   * @throws {LDAPError} If failed to bind to Active Directory
   */
  static async createClient(url, baseDN, domain, username, password) {
    try {
      let newClient = new this(url, baseDN, domain, username, password);
      await newClient.bind();
      return newClient;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Attempts to bind this ActiveDirectory connection object to the Active Directory server
   * @throws {LDAPError} If failed to bind to AD.
   */
  async bind() {
    let fqn = `${this._domain}\\${this._username}`;

    this._client.on("error", async err => {
      console.log(err.message);
      this._isConnected = false;
    });
    return new Promise((resolve, reject) => {
      let callback = err => {
        if (err) {
          reject(err);
        } else {
          this._isConnected = true;
          resolve();
        }
      };
      this._client.bind(fqn, this._password, callback);
    });
  }

  /**
   * Authenticates a username/password. Performs a simple ldap bind with the supplied username/password
   * @param {string} username SAMAccountName of user
   * @param {string} password Users
   * @returns {boolean} true if authentication is successful.
   */
  async authenticate(username, password) {
    try {
      this._isConnected || (await this.bind());
    } catch (err) {
      throw new Error("Failed to bind to Active Directory: " + err.message);
    }

    return new Promise((resolve, reject) => {
      let tempClient = ldapjs.createClient({
        url: this._url
      });
      let callback = (err, res) => {
        if (err) {
          if (err.code === LDAP_CODES.invalid_credentials) {
            resolve(false);
          } else {
            reject(
              `Error authenticating user: Error Name: ${err.name}, LDAP Error Code ${err.code}, Message: ${err.message}`
            );
          }
        } else {
          resolve(true);
        }
      };
      let fqn = `${this._domain}\\${username}`;
      tempClient.bind(fqn, password, callback);
    });
  }

  /**
   * Gets the direct members of a group.  Does not follow child groups
   * @param {string} groupname Name of LDAP group
   */
  async getDirectGroupMembers(groupname) {
    try {
      this._isConnected || (await this.bind());
    } catch (err) {
      throw new Error("Failed to bind to Active Directory: " + err.message);
    }

    try {
      let members = [];
      let group = await this.getGroup(groupname);

      if (!group || !group.members || !group.members.length) return null;
      for (var index = 0; index < group.members.length; index++) {
        var memberDN = group.members[index];
        members.push(await this.getObjectByDN(memberDN));
      }
      return members;
    } catch (error) {
      console.log(`Failed to retrive members of group ${groupname}. Reason: ${error.message}`);
      throw error;
    }
  }

  /**
   * Gets all users in a group. Follows child groups recusively. Returns users only.
   * @param {groupname} groupname Name of group ldap group
   */
  async getGroupUsers(groupname) {
    try {
      this._isConnected || (await this.bind());
    } catch (err) {
      throw new Error("Failed to bind to Active Directory: " + err.message);
    }

    try {
      let users = [];
      let members = await this.getDirectGroupMembers(groupname);

      if (!members || !members.length) return users;

      for (var index = 0; index < members.length; index++) {
        var member = members[index];

        if (member.objectType === LdapObjectType.user) {
          users.push(await this.getUser(member.accountName));
        } else if (member.objectType === LdapObjectType.group) {
          users = users.concat(await this.getGroupUsers(member.accountName));
        }
      }

      let usernames = [];
      let noDuplicates = [];
      users.forEach(user => {
        if (usernames.indexOf(user.username) === -1) {
          usernames.push(user.username);
          noDuplicates.push(user);
        }
      });
      return noDuplicates;
    } catch (error) {
      console.log(`Failed to get users of group ${groupname}. Reason: ${error.message}`);
      throw error;
    }
  }

  /**
   * Gets a User from Active Directory
   * @param {string} username SAMAccountName of user to fetch
   * @returns {User} A User object or null if no user is found
   */
  async getUser(username) {
    if (!username) return null;

    try {
      this._isConnected || (await this.bind());
    } catch (err) {
      throw new Error("Failed to bind to Active Directory: " + err.message);
    }

    try {
      let ldapquery = `(&(objectCategory=person)(objectClass=user)(samaccountname=${username}))`;
      let response = await this.search(ldapquery, LDAP_RETURN_ATTRIBUTES.user);

      if (!response.length) return null;
      if (response.length > 1)
        throw {
          message: "GetUser query returned more than one user.  This is.. not posible.",
          result: response
        };

      let user = {
        username: response[0].sAMAccountName,
        distinguishedName: response[0].distinguishedName,
        firstname: response[0].givenName,
        lastname: response[0].sn,
        name: response[0].name,
        email: response[0].mail,
        branch: response[0].physicalDeliveryOfficeName,
        telephone: response[0].telephoneNumber,
        ipPhone: response[0].ipPhone,
        mobilephone: response[0].mobile,
        title: response[0].title,
        department: response[0].department
      };

      return user;
    } catch (error) {
      console.log("getUser() ran into an error");
      console.log(error);

      //todo handle error
    }
  }

  /**
   * Gets a Group from Active Directory
   * @param {string} groupname SAMAccountName of group
   * @returns {Group} A Group object
   */
  async getGroup(groupname) {
    if (!groupname) return null;

    try {
      this._isConnected || (await this.bind());
    } catch (err) {
      throw new Error("Failed to bind to Active Directory: " + err.message);
    }

    try {
      let ldapquery = `(&(objectCategory=group)(cn=${groupname}))`;
      let response = await this.search(ldapquery, LDAP_RETURN_ATTRIBUTES.group);
      if (!response || !response.length) return null;
      if (response.length > 1)
        throw {
          message: "GetGroup query returned more than one group.  This is.. not posible.",
          result: response
        };

      let members = response[0].member;
      if (members === null || members === undefined) {
        members = [];
      } else if (!Array.isArray(members)) {
        members = [members];
      }
      let group = {
        dn: response[0].distinguishedName,
        type: response[0].groupType,
        email: response[0].sn,
        name: response[0].name,
        members: members
      };

      return group;
    } catch (error) {
      console.log("getGroup() ran into an error");
      console.log(error);

      //todo handle error
    }
  }

  /**
   * Gets the AccountName and Class of a given Distringuished Name
   * @param {string} dn The Distinguished Name of an object in Active Directory
   * @returns {object} An object with props: name, objectType(user, group, contact), SAMAccountName
   */
  async getObjectByDN(dn) {
    if (!dn) return null;

    try {
      this._isConnected || (await this.bind());
    } catch (err) {
      throw new Error("Failed to bind to Active Directory: " + err.message);
    }

    try {
      let ldapquery = `(distinguishedName=${dn})`;
      let response = await this.search(ldapquery, LDAP_RETURN_ATTRIBUTES.object);

      if (!response.length) return null;
      if (response.length > 1)
        throw {
          message: "getObjectByDN query returned more than one object.  This is.. not posible.",
          result: response
        };

      let thing = {
        name: response[0].cn,
        objectType: this.determineObjectType(response[0].objectCategory, response[0].objectClass),
        accountName: response[0].sAMAccountName
      };

      return thing;
    } catch (error) {
      console.log("getObjectByDN() ran into an error");
      console.log(error);

      //todo handle error
    }
  }

  determineObjectType(ldapCategory, ldapClass) {
    let paths = ldapCategory.split(",");
    let cn = paths[0].split("=")[1];

    if (cn === "Person" && ldapClass.indexOf("user") > -1) return LdapObjectType.user;
    else if (cn === "Person" && ldapClass.indexOf("contact") > -1) return LdapObjectType.contact;
    else if (cn === "Computer") return LdapObjectType.computer;
    else if (cn === "Group") return LdapObjectType.group;
    else if (cn === "OU") return LdapObjectType.ou;
    else
      console.log(
        "determineObjectType() couldnt determine a valid known object type. We only support a few main ones. returning null."
      );
    return null;
  }

  /**
   * Checks whether a user is a member of a group. Checks nested groups.
   * @param {string} username SAMAccountName of user
   * @param {string} groupname SAMAccountName of group
   */
  async userIsMemberOf(username, groupname) {
    try {
      this._isConnected || (await this.bind());
    } catch (err) {
      throw new Error("Failed to bind to Active Directory: " + err.message);
    }

    let users = await this.getGroupUsers(groupname);
    for (var index = 0; index < users.length; index++) {
      var user = users[index];
      if (user.username.toUpperCase() === username.trim().toUpperCase()) {
        return true;
      }
    }
    return false;
  }

  search(ldapquery, returnAttributes, baseDN, scope) {
    if (!scope) {
      scope = "sub";
    }

    let options = {
      scope: scope,
      filter: ldapquery,
      attributes: returnAttributes
    };

    let controls;

    return new Promise(async (resolve, reject) => {
      let result = [];
      let callback = (err, response) => {
        if (err) {
          reject(err);
        } else {
          response.on("searchEntry", function(entry) {
            result.push(entry.object);
          });
          response.on("error", function(err) {
            reject(err);
          });
          response.on("end", function(end) {
            if (end.status === 0) {
              resolve(result);
            } else {
              let error = {
                status: end.status,
                message: "LDAP search ended unsuccessfully (non 0 status)"
              };
              reject(error);
            }
          });
        }
      };

      try {
        this._isConnected || (await this.bind());
      } catch (err) {
        throw new Error("Failed to bind to Active Directory: " + err.message);
      }

      this._client.search(baseDN || this._baseDN, options, callback);
    });
  }
};
