var should = require("chai").should();
import ActiveDirectory from "../dist/index";

const VALID_URL = "ldap://vanderpdc2.internal.vanderfield.com.au:389";
const VALID_BASEDN = "DC=internal,DC=vanderfield,DC=com,DC=au";
const VALID_DOMAIN = "D230119";
const VALID_USERNAME = "mg0208";
const VALID_PASSWORD = "";
const INVALID_USERNAME = "fakeuser";
const INVALID_PASSWORD = "fakepass";
const VALID_GROUP = "IT_SG";
const INVALID_GROUP = "invalidGroup";

describe("ActiveDirectory", function() {
  describe("Creating a client...", function() {
    describe(".constructor()", function() {
      it("should return an object", function() {
        const AD1 = new ActiveDirectory(VALID_URL, VALID_BASEDN, VALID_DOMAIN, VALID_USERNAME, VALID_PASSWORD);
        AD1.should.be.a("object");
      });
    });

    describe(".bind()", function() {
      it("Should succeed in binding to Active Directory when valid params are passed", function(done) {
        const AD2 = new ActiveDirectory(VALID_URL, VALID_BASEDN, VALID_DOMAIN, VALID_USERNAME, VALID_PASSWORD);
        AD2.bind()
          .then(function() {
            done();
          })
          .catch(function(e) {
            done(e);
          });
      });

      it("Should fail to bind to Active Directory when invalid params are passed", function(done) {
        const AD3 = new ActiveDirectory(VALID_URL, VALID_BASEDN, VALID_DOMAIN, INVALID_USERNAME, INVALID_PASSWORD);
        AD3.bind()
          .then(function() {
            done(new Error("Should have failed to bind"));
          })
          .catch(function(e) {
            done();
          });
      });
    });

    describe(".createClient()", function() {
      it("Should succeed in binding to AD with valid params", function(done) {
        const AD4 = ActiveDirectory.createClient(VALID_URL, VALID_BASEDN, VALID_DOMAIN, VALID_USERNAME, VALID_PASSWORD)
          .then(function(obj) {
            done();
          })
          .catch(function(err) {
            done(err);
          });
      });

      it("Should throw an error on invalid params", function(done) {
        const AD4 = ActiveDirectory.createClient(
          VALID_URL,
          VALID_BASEDN,
          VALID_DOMAIN,
          INVALID_USERNAME,
          INVALID_PASSWORD
        )
          .then(function(obj) {
            done(new Error("Should have failed!"));
          })
          .catch(function(err) {
            done();
          });
      });
    });

    describe(".createClient() return value on successful bind", function() {
      var AD4;

      before(function(done) {
        ActiveDirectory.createClient(VALID_URL, VALID_BASEDN, VALID_DOMAIN, VALID_USERNAME, VALID_PASSWORD)
          .then(function(obj) {
            AD4 = obj;
            done();
          })
          .catch(function(e) {
            done(e);
          });
      });

      it("Should be an object", function() {
        AD4.should.be.a("object");
      });

      it("Should have username property", function() {
        AD4.should.have.property("username");
      });
      it("Should have password property", function() {
        AD4.should.have.property("password");
      });
      it("Should have url property", function() {
        AD4.should.have.property("url");
      });
      it("Should have baseDN property", function() {
        AD4.should.have.property("baseDN");
      });
    });
  });
  describe("API Calls...", function() {
    var AD;
    before(function(done) {
      ActiveDirectory.createClient(VALID_URL, VALID_BASEDN, VALID_DOMAIN, VALID_USERNAME, VALID_PASSWORD)
        .then(function(obj) {
          AD = obj;
          done();
        })
        .catch(function(e) {
          done(e);
        });
    });
    describe(".authenticate()", function() {
      it("Should return true on valid username/password", function(done) {
        AD.authenticate(VALID_USERNAME, VALID_PASSWORD)
          .then(function(val) {
            val === true ? done() : done(new Error("Should have returned true"));
          })
          .catch(function(error) {
            done(error);
          });
      });
      it("Should return false on invalid username/password", function(done) {
        AD.authenticate(INVALID_USERNAME, INVALID_PASSWORD)
          .then(function(val) {
            val === true ? done(new Error("Should have returned false")) : done();
          })
          .catch(function(error) {
            done(error);
          });
      });
    });
    describe(".getUser()", function() {
      const userProperties = [
        "username",
        "distinguishedName",
        "firstname",
        "lastname",
        "name",
        "email",
        "branch",
        "telephone",
        "ipPhone",
        "mobilephone",
        "title",
        "department"
      ];
      let user;
      before(function(done) {
        AD.getUser(VALID_USERNAME)
          .then(function(u) {
            user = u;
            user === null ? done(new Error("Should have found user!")) : done();
          })
          .catch(function(error) {
            done(error);
          });
      });
      it("Should return null if user does not exist", function(done) {
        AD.getUser(INVALID_USERNAME)
          .then(function(u) {
            u === null ? done() : done(new Error("Should have returned null"));
          })
          .catch(function(error) {
            done(error);
          });
      });

      it("Should return an object if user exists", function() {
        user.should.be.a("object");
      });

      for (let index in userProperties) {
        it(`User should have ${userProperties[index]} property`, function() {
          user.should.have.property(userProperties[index]);
        });
      }
    });

    describe(".getGroup()", function() {
      const groupProperties = ["dn", "name", "email", "type", "members"];
      let group;

      before(function(done) {
        AD.getGroup(VALID_GROUP)
          .then(function(g) {
            group = g;
            done();
          })
          .catch(function(error) {
            done(error);
          });
      });

      it("Should return null if group does not exist", function(done) {
        AD.getGroup(INVALID_GROUP)
          .then(function(g) {
            g === null ? done() : done(new Error());
          })
          .catch(function(error) {
            done(error);
          });
      });

      it("Should return an object if group exists", function() {
        group.should.be.a("object");
      });

      for (let index in groupProperties) {
        it(`Group should have ${groupProperties[index]} property`, function() {
          group.should.have.property(groupProperties[index]);
        });
      }
    });
  });
});
